<?php

$arr1 = array("pushpi","princy","baishali");
$arr2 = array("12205038","12205008","12205076");

$result = array_combine($arr1,$arr2);
print_r($result);

?>

<br/>

<?php

$arr3 = array("pushpi" => "12205038","princy" => "12205008","baishali" => "12205076");

$result1 = array_flip($arr3);
print_r($result1);

?>

<br/>

<?php

function test_odd($var)
{
    return ($var & 1);//here $var[$var means any number like 1,2,3] will be checked if it is 1 or not. If 1, then 1&1=true
}

$a1=array("a","b",2,3,4);
print_r(array_filter($a1,"test_odd"));

?>

<br/>

<?php

$a=array("Volvo"=>"XC90","BMW"=>"X5");
if (array_key_exists("Volvo",$a))
{
    echo "Key exists!";
}
else
{
    echo "Key does not exist!";

}

?>

<br/>

<?php

$a=array("Volvo"=>"XC90","BMW"=>"X5","Toyota"=>"Highlander");
print_r(array_keys($a));

?>

<br/>

<?php

$a=array("red","green");
print_r(array_pad($a,5,"blue"));//minimum 5 elements required. & if there is blank, then it'll be filled by "blue"
//+5 will add after the root array & -5 will add before the root array

?>

<br/>

<?php

$a=array("red","green");
print_r(array_pad($a,10,"orange"));

?>

<br/>

<?php

$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_merge($a1,$a2));

?>

<br/>

<?php

$a=array("red","green","blue");
array_pop($a);
print_r($a);

?>

<br/>

<?php

$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a);

?>

<br/>

<?php

$a1=array("red","green");
$a2=array("blue","yellow");

print_r(array_replace($a1,$a2));

?>

<br/>

<?php
//searches the array index
$a=array("red","green","blue");
echo array_search("green",$a).'<br/>';
echo array_search("red",$a).'<br/>';
echo array_search("blue",$a).'<br/>';

?>

<br>

<?php

$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($a));

?>

<br/>

<?php
//will take any 4 elements randomly. reloading will change the output.
$a=array("red","green","blue","yellow","brown");
$random_keys=array_rand($a,4);

echo $a[$random_keys[0]]."<br>";
echo $a[$random_keys[1]]."<br>";
echo $a[$random_keys[2]]."<br>";
echo $a[$random_keys[3]]."<br>";

?>

<br/>

<?php
//will replace & recursive the arrays. recursive means to call ownself

$a1=array("a"=>array("red"),"b"=>array("green","blue"),);
$a2=array("a"=>array("yellow"),"b"=>array("black"));

print_r(array_replace_recursive($a1,$a2));

?>

<br/>

<?php
//shifts the first element

$a=array("a"=>"red","b"=>"green","c"=>"blue");

echo array_shift($a) . '<br>';
print_r ($a);

?>

<br/>

<?php
//characters & strings are not allowed

$a=array(45.65,76.40,67.05);
echo array_sum($a);

?>
